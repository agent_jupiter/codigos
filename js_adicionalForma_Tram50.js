//script adicional ala forma a dictaminar 50 instancia 35  ->>01 16/05/20

form.getItems().forEach(function (b) {
    if (b.editorType == "text") {
        addFocusChanged(b);
    }
});
form.getItems().forEach(function (a) {
    if (a.editorType == "RichTextItem") {
        addFocusChanged(a);
    }
});

function addFocusChanged(item) {
    item.canvas.editArea.focusChanged = function (f) {
        if (f && !form.values[item.name]) {
            item.canvas.editArea.setSelectionFontSize(5);
        }
    }
}

//Visualizar
let paginaActual = 0;
let documento = null;
let cantidadPaginas = 2;

isc.Window.create({
    ID: "modalVisualizar",
    width: "60%",
    height: "948px",
    isModal: true,
    title: "Documento",
    autoCenter: true,
    showModalMask: true,
    showMinimizeButton: false,
    canDragReposition: false,
    autoDraw: false,
    members: [
        isc.HTMLFlow.create({
            ID: "documentoPanel",
            width: "100%",
            height: "100%",
            httpMethod: "POST",
            contentsType: "page",
            // contentsURL: window.location.origin + eng.contextPath + "/utils/getImagen.jsp?page="+imagenActual+"&uri=fd" + "/" + documento[0].id+"/" + documento[0].name + "&width="+1185+"&height="+948

        }),
        isc.VLayout.create({
            ID: "panelControlRight",
            width: "50%",
            members: [isc.HLayout.create({
                ID: "formPanelControlRight",
                members: [
                    isc.Button.create({
                        title: "", icon: eng.contextPath + "/platform/isomorphic/skins/Tahoe/images/actions/first.png",
                        click: function () {
                            paginaActual = 0;
                            paginaActual = paginaActual - 1;
                            documentoPanel.setContentsURL(window.location.origin + eng.contextPath + "/utils/getImagen.jsp?prueba=true&page=" + paginaActual + "&width=" + documentoPanel.getWidth() + "&height=" + documentoPanel.getHeight());
                            cantidadActualImagenes.setValue(paginaActual + 1)
                        }
                    }),
                    isc.Button.create({
                        title: "", icon: eng.contextPath + "/platform/isomorphic/skins/Tahoe/images/actions/back.png",
                        click: function () {
                            if (paginaActual != 0) {
                                paginaActual = paginaActual - 1;
                                documentoPanel.setContentsURL(window.location.origin + eng.contextPath + "/utils/getImagen.jsp?prueba=true&page=" + paginaActual + "&width=" + documentoPanel.getWidth() + "&height=" + documentoPanel.getHeight());
                                cantidadActualImagenes.setValue(paginaActual + 1);
                            }
                        }
                    }),
                    isc.Button.create({
                        title: "", icon: eng.contextPath + "/platform/isomorphic/skins/Tahoe/images/actions/next.png",
                        click: function () {
                            if (cantidadPaginas > (paginaActual + 1)) {
                                paginaActual = paginaActual + 1;
                                documentoPanel.setContentsURL(window.location.origin + eng.contextPath + "/utils/getImagen.jsp?prueba=true&page=" + paginaActual + "&width=" + documentoPanel.getWidth() + "&height=" + documentoPanel.getHeight());
                                cantidadActualImagenes.setValue(paginaActual + 1);
                            }
                        }
                    }),
                    isc.Button.create({
                        title: "", icon: eng.contextPath + "/platform/isomorphic/skins/Tahoe/images/actions/last.png",
                        click: function () {
                            paginaActual = cantidadPaginas - 1;
                            documentoPanel.setContentsURL(window.location.origin + eng.contextPath + "/utils/getImagen.jsp?prueba=true&page=" + paginaActual + "&width=" + documentoPanel.getWidth() + "&height=" + documentoPanel.getHeight());
                            cantidadActualImagenes.setValue(paginaActual + 1);
                        }
                    }),
                    isc.DynamicForm.create({
                        numCols: 6, fields: [
                            { title: "", ID: "cantidadActualImagenes", colSpan: 1, value: paginaActual + 1, showTitle: false, width: 40 },
                            { title: "", colSpan: 1, defaultValue: "de", showTitle: false, type: "StaticTextItem" },
                            { title: "", ID: "cantidadSelecionadosImagenes", colSpan: 1, value: cantidadPaginas, disabled: true, showTitle: false, width: 40 },]
                    }),
                ]
            })]
        })
        //})
    ]
});
function getModal() {
    documentoPanel.setContentsURL(window.location.origin + eng.contextPath + "/utils/getImagen.jsp?prueba=true&page=" + paginaActual + "&width=" + documentoPanel.getWidth() + "&height=" + documentoPanel.getHeight());
    modalVisualizar.show();
}

form.buttons.members[1].hide();
var request = null;
var solicitud = null;
var solicitudes = null;
var resoluciones = null;
var tramitePadre = eng.getDataSource("SolicitudesTramitesDireccionReserva").fetch({ data: { _id: form.values.solicitudesTramitesId } }).data[0];
this.onload = function () {
    setTimeout(function () {
        setTimeout(function () {
            if (!form.values.fechaResolucion) {
                form.getItem("fechaResolucion").setValue(new Date());
            }
        }, 1000);
        let resolucionItem = form.getItem("dicResolucion");
        let dictamenItem = form.getItem("dicDictaminar");
        let dictamenSel = dictamenItem.getValue();
        let resolucionSel = resolucionItem.getValue();
        resolucionItem.fetchData(function () {
            resolucionItem.storeValue(null);
            resolucionItem.storeValue(resolucionSel);
            var vm = resolucionItem.getValueMap();
            Object.keys(vm).forEach(function (a) {
                switch (vm[a]) {
                    case "DS":
                        var descStr = vm[a] + " - DESECHAMIENTO";
                        vm[a] = descStr;
                        break;
                    case "OK":
                        var descStr = vm[a] + " - TRÁMITE OTORGADO";
                        vm[a] = descStr;
                        break;
                    case "RQ":
                        var descStr = vm[a] + " - REQUERIMIENTO";
                        vm[a] = descStr;
                        break;
                    case "NG":
                        var descStr = vm[a] + " - NEGATIVA";
                        vm[a] = descStr;
                        break;
                    case "SP":
                        var descStr = vm[a] + " - SUSPENSIÓN";
                        vm[a] = descStr;
                        break;
                    case "DM":
                        var descStr = vm[a] + " - DESISTIMIENTO";
                        vm[a] = descStr;
                        break;
                    case "EC":
                        var descStr = vm[a] + " - EXPEDICIÓN DE COPIAS"; //Cambiar
                        vm[a] = descStr;
                        break;
                    case "CN":
                        var descStr = vm[a] + " - COPIAS NEGADAS";//Cambiar
                        vm[a] = descStr;
                        break;
                }
            });
            resolucionItem.setValueMap(vm);
            dictamenItem.fetchData(function () {
                dictamenItem.storeValue(null);
                dictamenItem.storeValue(dictamenSel);
            });

        });

        if (form.values.dicBuscarReserva) {
            form.getItem("dicBotonBuscarReserva").click(form, form.getItem("dicBotonBuscarReserva"));
        }


        if (resolucionItem.data)
            this.form.getField('dicBotonDetalle').disable();
        this.form.getField('precio').hide();
        this.form.getItem('descripcionTramiteADictaminar').hide();
        this.form.getItem('motivacion').hide();
        this.form.getItem('descripcionTramiteDictaminar').hide();
        //console.log(this.form.getField('solicitantes'));
        this.form.getField('solicitantes').setRequired(true);
        this.form.getField('dicBuscarReserva').setRequired(true);
        this.form.getField('dicMostrarTitulo').setRequired(true);
        this.form.getField('solicitantes').grid.setFields([{ name: "fullName", required: true }, { name: "rfc", required: true }]);
        this.form.getField('dicResolucion').getPickListFilterCriteria = function () {
            return { estatusId: ["CN", "DS", "RQ", "DM", "EC"] };
            console.log(this.form.fields);
        }
        this.form.getField('dicResolucion').pickListFields = [{
            name: "estatusId", width: 120, formatCellValue: function (value, record, rowNum, colNum) {
                if (value) {
                    switch (value) {
                        case "CN":
                            var descStr = value + " - COPIAS NEGADAS";

                            break;
                        case "DS":
                            var descStr = value + " - DESECHAMIENTO";

                            break;
                        case "EC":
                            var descStr = value + " - EXPEDICIÓN DE COPIAS";

                            break;
                        case "RQ":
                            var descStr = value + " - REQUERIMIENTO";

                            break;
                        case "DM":
                            var descStr = value + " - DESISTIMIENTO";

                            break;
                    }
                    //var descStr = value + " -AGREGANDO TEXTO";
                    return descStr;
                }
                return value;
            }
        }];
        this.form.getField('dicDictaminar').pickListFields = [{
            name: "descripcion", formatCellValue: function (value, record, rowNum, colNum) {
                if (value) {
                    //value = value.substring(2, value.length);
                    var descStr = value;
                    return descStr;
                }
                return value;
            },
        }];
        form.reorderField(4, 21);
        form.reorderField(4, 20);
        form.reorderField(4, 23);

        //console.log(form.values._id);
        solicitudes = eng.getDataSource("SolicitudesTramitesDireccionReserva");
        solicitud = solicitudes.fetch({ data: { "_id": form.values._id } }).data[0];
        //console.log(solicitud);
        var clone = JSON.parse(JSON.stringify(solicitud));
        request = { oldValues: clone };

        this.form.buttons.members[0].click = function (p1) {
            parent.loadContent("admin_content?pid=DictamenDictaminarReserva", ".content-wrapper");
        };
    }, 0)
    form.buttons.members[3].hide();
    form.buttons.members[4].hide();
    form.buttons.members[5].hide();
    //form.getField('dicDictaminar').pickListHeight();
}

let solicitudes_ = eng.getDataSource("SolicitudesTramitesDireccionReserva");
const GUARDAR = 0;
const ENVIAR = 1;
function actualizarDatos(tipo) {
    let suri = form.values._id;

    let data = {
        data: {
            _id: suri,
            estatusDictamen: form.values.dicResolucion,
            dictamenReserva: form.values.dicDictaminar
        }
    }
    //if(form.getItem("dicResolucion").getSelectedRecord().estatusId=="EC"){}

    if (tipo === GUARDAR) {

    } else if (tipo === ENVIAR) {
        data.data["estatus"] = "ENVIADO A VALIDACION";
        
    }
    solicitudes_.update(data);
}
form.buttons.addMember(isc.IButton.create({
    title: "Guardar",
    padding: "10px",
    click: function (p1) {
        let solRemoved = 0;
        for (let i = 0; i < form.getItem('solicitantes').grid.getTotalRows(); i++) {
            if (form.getItem('solicitantes').grid.recordMarkedAsRemoved(i) === true && this.form.getItem('rfc').getValue() !== null) {
                solRemoved++;
            }
        }
        let totalRows = form.getItem('solicitantes').grid.getTotalRows() - solRemoved;
        if (totalRows > 0) {
            form.getItem('solicitantes').setRequired(false);
            eng.submit(form, this, function () {
                actualizarDatos(GUARDAR);
                isc.say("Datos enviados correctamente")
            });
            form.getItem('solicitantes').setRequired(true);
        } else {
            isc.warn("Ingrese por lo menos un solicitante.")
        }

    }
}), 2);

form.buttons.addMember(isc.IButton.create({
    title: "Ver Oficio",
    padding: "10px",
    click: function (p1) {

        console.log("ver Oficio de expedición de copias");

    }
}), 3);

form.buttons.addMember(isc.IButton.create({
    ID: "verLeyenda",
    title: "Ver Leyenda",
    padding: "10px",
    click: function (p1) {

        console.log("Se visualiza la Leyenda de certificación de copias");

    }
}), 4);

function getNumeroOficio(estatusLetra, sol) {
    var dataOficio = eng.getDataSource("NumerosOficioReserva").add({
        //console.log(dataOficio);
        data: {
            resolucion: estatusLetra,
            solicitudesTramitesId: tramitePadre._id
        },
        componentId: "obtenerConsecutivo"
    });
    if (dataOficio.data && dataOficio.data._id) {
        var dat = {};
        dat["oficio"] = dataOficio.data._id;
        dat["numeroOficio"] = dataOficio.data.numeroOficio;
        return dat;
    }
    return false;
}

// function guardaFecha() {
//     let fecha = solicitud.fechaResolucion = new Date();
//     let fechaActual = fecha.getDate() + '-' + (fecha.getMonth() + 1) + '-' + fecha.getFullYear();
//     form.getItem("fechaResolucion").setValue(fechaActual);
//     console.log(fechaActual);
// }


form.buttons.addMember(isc.IButton.create({
    title: "Visualizar",
    padding: "10px",
    click: function (p1) {
       getModal();
    }
}), 5);


form.buttons.addMember(isc.IButton.create({
    title: "Enviar",
    padding: "10px",
    baseStyle: "buttonenviar",
    click: function (p1) {

        let solRemoved = 0;
        for (let i = 0; i < form.getItem('solicitantes').grid.getTotalRows(); i++) {
            if (form.getItem('solicitantes').grid.recordMarkedAsRemoved(i) === true) {
                solRemoved++;
            }
        }


        /*var folio = form.values.folio;*/

        isc.confirm("Confirmación de dictamen sobre la reserva número: " + form.getField("dicBuscarReserva").getValue() + " ", function (value) {
            if (value) {

                console.log("Valor es confirmado. . .");


                let totalRows = form.getItem('solicitantes').grid.getTotalRows() - solRemoved;
                if (totalRows > 0) {
                    var hasRFCnulls = false;
                    for (let i = 0; i < form.getItem('solicitantes').grid.getTotalRows(); i++) {
                        if (form.getItem('solicitantes').grid.recordMarkedAsRemoved(i) === false) {
                            if (form.getItem('solicitantes').grid.getEditedRecord(i).rfc === null) {
                                hasRFCnulls = true;
                            }
                        }
                    }
                    if (hasRFCnulls === false) {
                        eng.submit(form, this, function () {
                            actualizarDatos(ENVIAR);
                            let precioReserva = eng.getDataSource("PreciosReserva").fetch({ data: { "precioId": 8 } }).data[0].precio;
                            var solicitudes = eng.getDataSource("SolicitudesTramitesDireccionReserva");
                            var solicitud = solicitudes.fetch({ data: { "_id": form.values.solicitudesTramitesId } }).data[0];
                            var clone = JSON.parse(JSON.stringify(solicitud));
                            request = { oldValues: clone };
                            solicitud.estatus = "ENVIADO A VALIDAR";
                            solicitud.estatusDictamen2 = "";

                            //Calculando el precio del tramite
                            let numCopiasCertificadas = this.form.getField('dicNumCopiasCertificadas').getValue();
                            let numCopiasSimples = this.form.getField('dicNumCopiasSimples').getValue();
                            if (this.form.getField('dicNumCopiasCertificadas').getValue() === undefined || this.form.getField('dicNumCopiasCertificadas').getValue() === null) { numCopiasCertificadas = 0 };
                            if (this.form.getField('dicNumCopiasSimples').getValue() === undefined || this.form.getField('dicNumCopiasSimples').getValue() === null) { numCopiasSimples = 0 };
                            let sum = (numCopiasCertificadas + numCopiasSimples) * precioReserva;
                            solicitud.precio = sum;
                            request.data = solicitud;
                            var dataI = solicitudes.update(request);

                            console.log("Enviado... ");
                            //value = form.getItem("dicResolucion").getSelectedRecord();
                            // let valor = form.values.dicResolucion.split("-", 1);
                            // value = valor[0];
                            // console.log("value");
                            // console.log(value);
                            let value = form.getItem("dicResolucion").getSelectedRecord();
                            if (value) {
                                //if(["CN", "DS", "RQ","DM", "EC"].some(value.estatusId)){}
                                //var numeroOficio = getNumeroOficio(value.estatusId);
                                var numeroOficio = getNumeroOficio(value.estatusId);
                                if (numeroOficio) {
                                    isc.say("El número de oficio es: " + numeroOficio.numeroOficio, function () {
                                        request.data["numeroOficio"] = numeroOficio.numeroOficio;
                                        solicitudes.update(request);
                                        //metodo guardar fecha
                                        //  guardaFecha();
                                        //console.log(dataI);
                                        if (clone.estatusDictamen2 == "corregir") {
                                            parent.loadContent("admin_content?pid=BandejaDictamenCorregirReserva", ".content-wrapper");
                                        } else {
                                            parent.loadContent("admin_content?pid=DictamenDictaminarReserva", ".content-wrapper");
                                        }
                                    });
                                } else {
                                    return;
                                }

                            }



                            return false;
                        });
                    } else {
                        isc.warn("Ingrese el RFC.")
                    }
                } else {
                    isc.warn("Ingrese por lo menos un solicitante.")
                }
            } else {
                console.log("Valor es Cancelado. . .")
                isc.say("Dictamen cancelado ");
            }
        });
    }
}), 6);